import csso from 'postcss-csso'

export const plugins = [
  csso({
    restructure: false
  })
]
