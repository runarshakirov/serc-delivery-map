module.exports = {
  productionSourceMap: false,
  chainWebpack: (config) => {
    config.optimization.delete('splitChunks')
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/assets/_variables.scss";
          `
        // @import "@/assets/normolize.scss";
        // @import "@/assets/_fonts.scss";
      }
    }
  }
}
