// eslint-disable-next-line no-unused-vars
const brandStores = [
  {
    City: 'Москва',
    StoreName: 'ТРЦ Фестиваль',
    StoreAddress: 'Мичуринский Проспект, Олимпийская Деревня, 3к1',
    Coordinates: '55.677956, 37.466646'
  },
  {
    City: 'Москва',
    StoreName: 'ТРЦ Калейдоскоп',
    StoreAddress: 'ул.Сходненская, 56',
    Coordinates: '55.850408, 37.444090'
  },
  {
    City: 'Москва',
    StoreName: 'ТРЦ Авиапарк',
    StoreAddress: 'Ходынский бульвар, 4',
    Coordinates: '55.790231, 37.531289'
  },
  {
    City: 'Москва',
    StoreName: 'Сервисный центр Samsung Плаза Тверская',
    StoreAddress: 'Тверская улица, 22',
    Coordinates: '55.767423, 37.601178'
  },
  {
    City: 'Москва',
    StoreName: 'ТРЦ Атриум',
    StoreAddress: 'ул. Земляной вал, д.33, ТРК Атриум, 1 этаж, павильон 01-05',
    Coordinates: '55.757339, 37.659173'
  },
  {
    City: 'Москва',
    StoreName: 'ТРЦ Мега',
    StoreAddress:
      'г. Котельники, 1-й Покровский проезд, д.5, ТЦ Мега, 1 этаж, павильон 4114',
    Coordinates: '55.653299, 37.844217'
  },
  {
    City: 'Москва',
    StoreName: 'ТРЦ Афимолл Сити',
    StoreAddress: 'Пресненская набережная, 2',
    Coordinates: '55.749162, 37.539742'
  },
  {
    City: 'Москва',
    StoreName: 'ТЦ Черёмушки',
    StoreAddress: 'Профсоюзная ул., 56',
    Coordinates: '55.670021, 37.552480'
  },
  {
    City: 'Мытищи',
    StoreName: 'ТРЦ Красный кит',
    StoreAddress: 'Шараповский проезд, вл2',
    Coordinates: '55.916678, 37.759264'
  },
  {
    City: 'Мытищи',
    StoreName: 'ТРЦ Июнь',
    StoreAddress: 'улица Мира, с51',
    Coordinates: '55.919962, 37.708491'
  },
  {
    City: 'Реутов',
    StoreName: 'ТЦ Шоколад',
    StoreAddress: 'МКАД, 2-й километр, 2',
    Coordinates: '55.763807, 37.844990'
  },
  {
    City: 'Орехово-Зуево',
    StoreName: 'ТЦ Капитолий',
    StoreAddress: 'улица Якова Флиера, 4',
    Coordinates: '55.804607, 38.974316'
  },
  {
    City: 'Сергиев Посад',
    StoreName: 'ТЦ Капитолий',
    StoreAddress: 'Новоугличское шоссе, 85',
    Coordinates: '56.339821, 38.124617'
  },
  {
    City: 'Зеленоград',
    StoreName: 'ТРЦ Иридиум',
    StoreAddress: 'Крюковская площадь, 1',
    Coordinates: '55.982668, 37.175008'
  },
  {
    City: 'Санкт-Петербург',
    StoreName: 'ТРЦ Родео Драйв',
    StoreAddress: 'проспект Культуры, 1',
    Coordinates: '60.033706, 30.367755'
  },
  {
    City: 'Санкт-Петербург',
    StoreName: 'ТРЦ Питер Радуга',
    StoreAddress: 'проспект Космонавтов, 14',
    Coordinates: '59.868948, 30.350489'
  },
  {
    City: 'Санкт-Петербург',
    StoreName: 'ТРЦ Жемчужная плаза',
    StoreAddress: 'Петергофское шоссе, 51А',
    Coordinates: '59.849831, 30.144982'
  },
  {
    City: 'Санкт-Петербург',
    StoreName: 'ТЦ Невский центр',
    StoreAddress: 'Невский проспект, 114-116',
    Coordinates: '59.931941, 30.359149'
  },
  {
    City: 'Санкт-Петербург',
    StoreName: 'ТРЦ Охта Молл',
    StoreAddress: 'Брантовская дорога, 3',
    Coordinates: '59.940150, 30.418366'
  },
  {
    City: 'Санкт-Петербург',
    StoreName: 'ТРЦ Галерея',
    StoreAddress: 'Лиговский проспект, 30',
    Coordinates: '59.927563, 30.360613'
  }
]

export { brandStores }
