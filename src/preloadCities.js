// eslint-disable-next-line no-unused-vars
const preloadCities = [
  {
    data: {
      city: 'Москва',
      settlement: null,
      geo_lat: '55.75396',
      geo_lon: '37.620393'
    },
    value: 'г Москва'
  },
  {
    data: {
      city: 'Санкт-Петербург',
      settlement: null,
      geo_lat: '59.939099',
      geo_lon: '30.315877'
    },
    value: 'г Санкт-Петербург'
  },
  {
    data: {
      city: 'Екатеринбург',
      settlement: null,
      geo_lat: '56.838011',
      geo_lon: '60.597474'
    },
    value: 'г Екатеринбург'
  },
  {
    data: {
      city: 'Нижний Новгород',
      settlement: null,
      geo_lat: '56.326797',
      geo_lon: '44.006516'
    },
    value: 'г Нижний Новгород'
  },
  {
    data: {
      city: 'Казань',
      settlement: null,
      geo_lat: '55.796127',
      geo_lon: '49.106414'
    },
    value: 'г Казань'
  },
  {
    data: {
      city: 'Самара',
      settlement: null,
      geo_lat: '53.195878',
      geo_lon: '50.100202'
    },
    value: 'г Самара'
  }
]

export { preloadCities }
