import Vue from 'vue'
import App from './App.vue'
import YmapPlugin from 'vue-yandex-maps'
import TextHighlight from 'vue-text-highlight'

Vue.config.productionTip = false

Vue.component('text-highlight', TextHighlight)

const settings = {
  apiKey: '8b56a857-f05f-4dc6-a91b-bc58f302ff21',
  lang: 'ru_RU',
  coordorder: 'latlong',
  enterprise: false,
  version: '2.1'
}

Vue.use(YmapPlugin, settings)

Vue.directive('focus', {
  inserted: function (el) {
    el.focus()
  }
})

Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      // here I check that click was outside the el and his children
      if (!(el === event.target || el.contains(event.target))) {
        // and if it did, call method provided in attribute value
        vnode.context[binding.expression](event)
      }
    }
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  }
})

new Vue({
  render: (h) => h(App)
}).$mount('#serc-delivery-page')
